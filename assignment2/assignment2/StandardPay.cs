﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace assignment2
{
    public class StandardPay
    {
        private string employee;
        private double m_hours;
        private double m_hourlyrate;
        

         private StandardPay()
        {
        }

     public StandardPay(string employee, double hours, double hourlyrate)
        {
           this.employee = employee;
           m_hours = hours;
           m_hourlyrate = hourlyrate;

        }


     public string getEmployee()
    {
        return this.employee; 
    }

     public double hours
     {
         get { return m_hours; }
     }

     public double hourlyrate
     {
         get { return m_hourlyrate; }
     }



     public double getWages()
     {
         return hours * hourlyrate;

     }


     public double getNI()
     {
         return (hours * hourlyrate)/100 *5;


     }

     public double wagesNI()
     {
         return getWages() - getNI();
     }

     public void NI(double niamount)
     {
         niamount = (hours * hourlyrate) / 100 * 5;

         if (niamount < 20.00)
         {
             Console.WriteLine("niamount not to be paid due to not enough money earned this week");
         }
        
         if (niamount > 20.00)
         {
             Console.WriteLine("niamount");
         }

     }

    
 /*public static void Main()
        {
            StandardPay cr = new StandardPay("Chris Road", 37.00, 12.85);
            
            cr.Wages(37.00 * 12.85);
            cr.NI((37.00 * 12.85)/100 *5);
            
            Console.WriteLine("This week number of hours worked is", cr.hours);
        }*/




    }
}
