﻿namespace assignment2
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnAdd = new System.Windows.Forms.Button();
            this.EmpHR = new System.Windows.Forms.Label();
            this.EmpNI = new System.Windows.Forms.Label();
            this.EmpDOB = new System.Windows.Forms.Label();
            this.EmpName = new System.Windows.Forms.Label();
            this.EmpNo = new System.Windows.Forms.Label();
            this.txtHR = new System.Windows.Forms.TextBox();
            this.txtHoursWorked = new System.Windows.Forms.TextBox();
            this.txtDOB = new System.Windows.Forms.TextBox();
            this.txtName = new System.Windows.Forms.TextBox();
            this.txtEmpno = new System.Windows.Forms.TextBox();
            this.lbxEmployeeWages = new System.Windows.Forms.ListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtBonus = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // btnAdd
            // 
            this.btnAdd.Location = new System.Drawing.Point(158, 214);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(75, 23);
            this.btnAdd.TabIndex = 23;
            this.btnAdd.Text = "Add";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // EmpHR
            // 
            this.EmpHR.AutoSize = true;
            this.EmpHR.Location = new System.Drawing.Point(-140, 152);
            this.EmpHR.Name = "EmpHR";
            this.EmpHR.Size = new System.Drawing.Size(63, 13);
            this.EmpHR.TabIndex = 22;
            this.EmpHR.Text = "Hourly Rate";
            // 
            // EmpNI
            // 
            this.EmpNI.AutoSize = true;
            this.EmpNI.Location = new System.Drawing.Point(-135, 126);
            this.EmpNI.Name = "EmpNI";
            this.EmpNI.Size = new System.Drawing.Size(58, 13);
            this.EmpNI.TabIndex = 21;
            this.EmpNI.Text = "NI Number";
            // 
            // EmpDOB
            // 
            this.EmpDOB.AutoSize = true;
            this.EmpDOB.Location = new System.Drawing.Point(-143, 99);
            this.EmpDOB.Name = "EmpDOB";
            this.EmpDOB.Size = new System.Drawing.Size(66, 13);
            this.EmpDOB.TabIndex = 20;
            this.EmpDOB.Text = "Date of Birth";
            // 
            // EmpName
            // 
            this.EmpName.AutoSize = true;
            this.EmpName.Location = new System.Drawing.Point(-161, 73);
            this.EmpName.Name = "EmpName";
            this.EmpName.Size = new System.Drawing.Size(84, 13);
            this.EmpName.TabIndex = 19;
            this.EmpName.Text = "Employee Name";
            // 
            // EmpNo
            // 
            this.EmpNo.AutoSize = true;
            this.EmpNo.Location = new System.Drawing.Point(-150, 47);
            this.EmpNo.Name = "EmpNo";
            this.EmpNo.Size = new System.Drawing.Size(73, 13);
            this.EmpNo.TabIndex = 18;
            this.EmpNo.Text = "Employee No.";
            // 
            // txtHR
            // 
            this.txtHR.Location = new System.Drawing.Point(158, 149);
            this.txtHR.Name = "txtHR";
            this.txtHR.Size = new System.Drawing.Size(100, 20);
            this.txtHR.TabIndex = 17;
            // 
            // txtHoursWorked
            // 
            this.txtHoursWorked.Location = new System.Drawing.Point(158, 122);
            this.txtHoursWorked.Name = "txtHoursWorked";
            this.txtHoursWorked.Size = new System.Drawing.Size(100, 20);
            this.txtHoursWorked.TabIndex = 16;
            // 
            // txtDOB
            // 
            this.txtDOB.Location = new System.Drawing.Point(158, 96);
            this.txtDOB.Name = "txtDOB";
            this.txtDOB.Size = new System.Drawing.Size(100, 20);
            this.txtDOB.TabIndex = 15;
            // 
            // txtName
            // 
            this.txtName.Location = new System.Drawing.Point(158, 70);
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(100, 20);
            this.txtName.TabIndex = 14;
            // 
            // txtEmpno
            // 
            this.txtEmpno.Location = new System.Drawing.Point(158, 44);
            this.txtEmpno.Name = "txtEmpno";
            this.txtEmpno.Size = new System.Drawing.Size(100, 20);
            this.txtEmpno.TabIndex = 13;
            // 
            // lbxEmployeeWages
            // 
            this.lbxEmployeeWages.FormattingEnabled = true;
            this.lbxEmployeeWages.Location = new System.Drawing.Point(297, 12);
            this.lbxEmployeeWages.Name = "lbxEmployeeWages";
            this.lbxEmployeeWages.Size = new System.Drawing.Size(466, 147);
            this.lbxEmployeeWages.TabIndex = 12;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(77, 152);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(63, 13);
            this.label1.TabIndex = 28;
            this.label1.Text = "Hourly Rate";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(64, 125);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(76, 13);
            this.label2.TabIndex = 27;
            this.label2.Text = "Hours Worked";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(74, 99);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(66, 13);
            this.label3.TabIndex = 26;
            this.label3.Text = "Date of Birth";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(56, 73);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(84, 13);
            this.label4.TabIndex = 25;
            this.label4.Text = "Employee Name";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(67, 47);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(73, 13);
            this.label5.TabIndex = 24;
            this.label5.Text = "Employee No.";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(64, 178);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(76, 13);
            this.label6.TabIndex = 29;
            this.label6.Text = "Bonus Amount";
            // 
            // txtBonus
            // 
            this.txtBonus.Location = new System.Drawing.Point(158, 176);
            this.txtBonus.Name = "txtBonus";
            this.txtBonus.Size = new System.Drawing.Size(100, 20);
            this.txtBonus.TabIndex = 30;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(837, 350);
            this.Controls.Add(this.txtBonus);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.btnAdd);
            this.Controls.Add(this.EmpHR);
            this.Controls.Add(this.EmpNI);
            this.Controls.Add(this.EmpDOB);
            this.Controls.Add(this.EmpName);
            this.Controls.Add(this.EmpNo);
            this.Controls.Add(this.txtHR);
            this.Controls.Add(this.txtHoursWorked);
            this.Controls.Add(this.txtDOB);
            this.Controls.Add(this.txtName);
            this.Controls.Add(this.txtEmpno);
            this.Controls.Add(this.lbxEmployeeWages);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Label EmpHR;
        private System.Windows.Forms.Label EmpNI;
        private System.Windows.Forms.Label EmpDOB;
        private System.Windows.Forms.Label EmpName;
        private System.Windows.Forms.Label EmpNo;
        private System.Windows.Forms.TextBox txtHR;
        private System.Windows.Forms.TextBox txtHoursWorked;
        private System.Windows.Forms.TextBox txtDOB;
        private System.Windows.Forms.TextBox txtName;
        private System.Windows.Forms.TextBox txtEmpno;
        private System.Windows.Forms.ListBox lbxEmployeeWages;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtBonus;
        
    }
}

