﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlServerCe;

namespace assignment2
{
    public partial class Form1 : Form
    {
        SqlCeConnection mySqlConnection;


        public Form1()
        {

            InitializeComponent();
            populateListBox();
        }

        public void populateListBox()
        {
            mySqlConnection =
                    new SqlCeConnection(@"Data Source=E:\Uni Level 6\Software 1\Assignment\Assignment 2\MyDatabase#1.sdf");

            String selcmd = "SELECT employee_no, employee_name, employee_dob, employee_hours, employee_hourlyrate, employee_bonus FROM EmployeeWages ORDER BY employee_no";

            SqlCeCommand mySqlCommand = new SqlCeCommand(selcmd, mySqlConnection);

            try
            {
                mySqlConnection.Open();

                SqlCeDataReader mySqlDataReader = mySqlCommand.ExecuteReader();

                lbxEmployeeWages.Items.Clear();

                while (mySqlDataReader.Read())
                {

                    lbxEmployeeWages.Items.Add(mySqlDataReader["employee_no"] + " " +
                         mySqlDataReader["employee_name"] + " " + mySqlDataReader["employee_dob"] + " " + mySqlDataReader["employee_hours"] + " " + mySqlDataReader["employee_hourlyrate"]+ " " + mySqlDataReader["employee_bonus"]);


                }
            }

            catch (SqlCeException ex)
            {

                MessageBox.Show(EmpNo + " .." + ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }



        public void cleartxtBoxes()
        {
            txtEmpno.Text = txtName.Text = txtDOB.Text = txtHoursWorked.Text = txtHR.Text = txtBonus.Text = "";
        }

        public bool checkInputs()
        {
            bool rtnvalue = true;

            if (string.IsNullOrEmpty(txtEmpno.Text) ||
                string.IsNullOrEmpty(txtName.Text) ||
                string.IsNullOrEmpty(txtDOB.Text) ||
                string.IsNullOrEmpty(txtHoursWorked.Text) ||
                string.IsNullOrEmpty(txtHR.Text) ||
                string.IsNullOrEmpty(txtBonus.Text))
            {
                MessageBox.Show("Error: Please check your inputs");
                rtnvalue = false;
            }

            return (rtnvalue);

        }


        public void insertRecord( String employeeNo, String name, String DOB, String hours, String hourlyrate , String bonus, String commandString)
        {

            try
            {
                SqlCeCommand cmdInsert = new SqlCeCommand(commandString, mySqlConnection);

                cmdInsert.Parameters.AddWithValue("@employeeNo", employeeNo);
                cmdInsert.Parameters.AddWithValue("@name", name);
                cmdInsert.Parameters.AddWithValue("@DOB", DOB);
                cmdInsert.Parameters.AddWithValue("@HoursWorked", hours);
                cmdInsert.Parameters.AddWithValue("@HR", hourlyrate);
                cmdInsert.Parameters.AddWithValue("@Bonus", bonus);
                cmdInsert.ExecuteNonQuery();
            }
            catch (SqlCeException ex)
            {
                MessageBox.Show(employeeNo + " .." + ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }


        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (checkInputs())
            {

                String commandString = "INSERT INTO EmployeeWages (employee_no, employee_name, employee_dob, employee_hours, employee_hourlyrate, employee_bonus) VALUES (@employeeNo, @name, @DOB, @HoursWorked, @HR, @Bonus)";

                
                insertRecord(txtEmpno.Text, txtName.Text, txtDOB.Text, txtHoursWorked.Text, txtHR.Text, txtBonus.Text, commandString);
                populateListBox();
                cleartxtBoxes();
            }

        }
    }
}
