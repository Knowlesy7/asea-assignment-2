﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace assignment2
{
    public class Bonus
    {
        //StandardPay getEmployee();
        private StandardPay currentEmployee;
        private double sales;
        private double bonuses;
        private double wages;
        //StandardPay wagesNI();
        
       
           private Bonus()
        {
        }

        

        public Bonus(StandardPay employee, double sales, double bonuses, double wages)
        {
            this.currentEmployee = employee;
            this.sales = sales;
            this.bonuses = bonuses ;
            this.wages = wages;            

        }

        public double getSales()
        {
            return this.sales;
        }


        public void DueBonus()
        {


            if (sales > 200.00)
            {
                Console.WriteLine("You have have acquired enough sales this month to entitle you to a £150 bonus, well done keep up the good work");
            }

            else
            {
                Console.WriteLine("No Bonus this month keep trying and hopefully you'll get one next month");
            }


        }

        public double getTotalwages()
        {
            if (sales > 200.00)
            {
                 return wages + bonuses;
            }

            else
            {
                return wages;
            }


        }
    }

}
