﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace assignment2
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());

            StandardPay employee = new StandardPay("Chris Road", 37.00, 12.85);
            
            employee.getWages();
            employee.getNI();

            Console.WriteLine("This week number of hours worked is", employee.hours);

            Bonus bonus = new Bonus(employee, 250.00, 150.00, employee.getWages());
            bonus.getSales();
            bonus.getTotalwages();

            Console.WriteLine("This week you will be paid", bonus.getTotalwages());


        }
    }
}
