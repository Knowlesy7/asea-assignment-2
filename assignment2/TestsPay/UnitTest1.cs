﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using assignment2;

namespace TestsPay
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            //arrange

            double hours = 37.00;
            double hourlyrate = 9.65;
            double expectedwage = 357.05;
            StandardPay tomk = new StandardPay("Tom Knowles", hours, hourlyrate);

            //act
            tomk.getWages();

            double actual = tomk.getWages();
            Assert.AreEqual(expectedwage, actual, 0.001, "Wages incorrectly calculated");
        }

        [TestMethod]
        public void TestMethod2()
        {
            //arrange
            double hours = 37.00;
            double hourlyrate = 9.65;
            StandardPay tomk = new StandardPay("Tom Knowles", hours, hourlyrate);
 
            double sales = 258.00;
            double bonus = 150.00;
            double wages = tomk.getWages();
            Bonus tkbonus = new Bonus(tomk, sales, bonus, wages);
            
            double expected = tomk.getWages() + bonus;

            //act

            tomk.getWages();

             if (sales > 200.00){
             
                    double actual= tomk.getWages() + bonus;
                    Console.WriteLine("Bonus achieved");
             }
             else{
                
                   double actual = tomk.getWages();
                   Console.WriteLine("Bonus Not achieved, good luck next week");
             }

                            
            }

        }

    }

